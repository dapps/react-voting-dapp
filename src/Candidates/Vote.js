/* @flow */

import React, { Component } from 'react';
import { View, Button, StyleSheet } from 'react-native';
import Star from './Star';

import type { CandidateType } from './CandidateList';
import withWeb3 from '../withWeb3';

class Container extends Component<{
	Voting: *,
	web3: *,
	candidate: CandidateType,
}> {
	onPress = (candidate: string, star: number) => {
		this.voteForCandidate(candidate, star);
	};
	voteForCandidate: Function;
	voteForCandidate = (candidate: string, star: number) => {
		const { Voting } = this.props;
		const voteTokens = star * 2;
		console.log(
			'Vote has been submitted. The vote count will increment as soon as the vote is recorded on the blockchain. Please wait.'
		);

		Voting.deployed().then(contractInstance => {
			contractInstance
				.voteForCandidate(candidate, voteTokens, {
					gas: 140000,
					// using coinbase
					from: this.props.web3.eth.accounts[0],
				})
				.then(() => {
					return contractInstance.totalVotesFor.call(candidate).then(v => {
						console.log(v.toString());
					});
				});
		});
	};

	render() {
		const { candidate } = this.props;
		return (
			<View style={styles.Container}>
				<View style={styles.Content}>
					<Star onPress={args => this.onPress(candidate.hash, args)} />
				</View>
			</View>
		);
	}
}

export default withWeb3(Container);

const styles = StyleSheet.create({
	Container: {
		flex: 1,
	},
	Content: {
		paddingHorizontal: 10,
		// backgroundColor: '#336699',
	},
});
