/* @flow */

import React, { Component } from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';

import { material } from 'react-native-typography';
import withWeb3 from '../withWeb3';

class TokenData extends Component<
	{
		Voting: *,
		web3: *,
	},
	{
		price: number,
		sold?: *,
		total?: *,
		balance?: *,
	}
> {
	state = {
		price: 0,
	};
	componentDidMount() {
		// this.buyTokens(100);
		setInterval(() => {
			this.populateTokenData();
		}, 10000);
	}
	populateTokenData: Function;
	populateTokenData = () => {
		const { Voting } = this.props;
		return Voting.deployed().then(contractInstance => {
			return Promise.all([
				contractInstance.totalTokens().then(v => {
					this.setState({
						total: v.toString(),
					});
				}),
				contractInstance.tokensSold.call().then(v => {
					this.setState({
						sold: v.toString(),
					});
				}),
				contractInstance.tokenPrice().then(v => {
					const price = parseFloat(this.props.web3.fromWei(v.toString()));
					this.setState({
						price,
					});
				}),
				new Promise((resolve, reject) => {
					this.props.web3.eth.getBalance(
						contractInstance.address,
						(error, result) => {
							if (error) {
								return reject(error);
							}
							return resolve({
								balance: this.props.web3.fromWei(result.toString()) + ' Ether',
							});
						}
					);
				}).then(({ balance }) => this.setState({ balance })),
			]);
		});
	};

	/* The user enters the total no. of tokens to buy. We calculate the total cost and send it in
    * the request. We have to send the value in Wei. So, we use the toWei helper method to convert
    * from Ether to Wei.
    */
	buyTokens: Function;
	buyTokens = (tokensToBuy: number) => {
		const price = tokensToBuy * this.state.price;
		console.log('Purchase order has been submitted. Please wait.');
		const { Voting } = this.props;
		Voting.deployed().then(contractInstance => {
			contractInstance
				.buy({
					value: this.props.web3.toWei(price, 'ether'),
					from: this.props.web3.eth.accounts[0],
				})
				.then(v => {
					console.log(v);
				});
		});
		this.populateTokenData();
	};

	render() {
		return (
			<View style={styles.Container}>
				<View style={styles.Header}>
					<Text style={material.captionWhite}>
						{JSON.stringify(this.state)}
					</Text>
				</View>
				<View style={styles.buyButton}>
					<Button
						color="#78909C"
						accessibilityLabel="Learn more about buy token"
						onPress={() => this.buyTokens(100)}
						title={'Buy 100 tokens!'}
					/>
				</View>
			</View>
		);
	}
}

export default withWeb3(TokenData);

const styles = StyleSheet.create({
	Container: {
		flex: 1,
	},
	buyButton: {
		// margin: 10,
	},
	Header: {
		paddingVertical: 20,
		paddingHorizontal: 12,
		backgroundColor: '#336699',
	},
});
