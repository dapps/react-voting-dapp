/* @flow */

import React, { PureComponent } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { material } from 'react-native-typography';
import Vote from './Vote';

type Props = {
	candidates: Array<*>,
	Voting: *,
};

export type CandidateType = {
	hash: string,
	name: string,
	votes: string,
};

const Item = ({
	candidate,
	Voting,
}: {
	candidate: CandidateType,
	Voting: *,
}) => {
	return (
		<View>
			<View style={styles.row}>
				<Text style={[material.caption, styles.caption]}>{candidate.name}</Text>
				<Text style={[material.caption, styles.caption]}>
					{candidate.votes}
				</Text>
			</View>
			<Vote Voting={Voting} candidate={candidate} />
		</View>
	);
};
export default class CandidateList extends PureComponent<Props> {
	render() {
		return (
			<View>
				<View style={styles.header}>
					<Text style={material.title}>Rate your favourite super heroe...</Text>
				</View>
				{this.props.candidates.map(candidate => (
					<Item
						key={candidate.hash}
						Voting={this.props.Voting}
						candidate={candidate}
					/>
				))}
			</View>
		);
	}
}
const styles = StyleSheet.create({
	caption: {
		fontSize: 17,
	},
	header: {
		paddingHorizontal: 5,
		paddingVertical: 12,
		marginHorizontal: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	row: {
		paddingVertical: 12,
		borderTopWidth: 1,
		borderTopColor: '#CCC',
		marginHorizontal: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
});
