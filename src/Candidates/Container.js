/* @flow */

import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';

import { material } from 'react-native-typography';

class Container extends Component<{
	children: *,
}> {
	render() {
		return (
			<View style={styles.Container}>
				<View style={styles.Header}>
					<Text style={material.body1White} >
						A simple voting decentralized dapp, load 100 tokens before starting the rate and enjoy!
					</Text>
				</View>
				<Text style={[styles.title, material.caption]}>
					react-voting-dapp in the ethereum blockchain
				</Text>
				{this.props.children}
			</View>
		);
	}
}

export default Container;

const styles = StyleSheet.create({
	Container: {
		flex: 1,
	},
	Header: {
		paddingVertical: 20,
		paddingHorizontal: 12,
		backgroundColor: '#263238',
	},
	title: {
		paddingHorizontal: 12,
		paddingVertical: 9,
		backgroundColor: '#B0BEC5',
	},
});
