/* @flow */

import React, { Component } from 'react';
import ReactStars from 'react-stars';

class Star extends Component<{
	onPress: Function,
}> {
	onPress = args => {
		this.props.onPress(args);
	};

	render() {
		return (
			<ReactStars
				count={5}
				onChange={this.onPress}
				size={24}
				color2={'#ffd700'}
			/>
		);
	}
}

export default Star;
