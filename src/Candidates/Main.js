/* @flow */

import React, { Component } from 'react';
import CandidateList from './CandidateList';
import TokenData from './TokenData';

import Contract from 'truffle-contract';
import { View, StyleSheet } from 'react-native';

import withWeb3 from '../withWeb3';
import type { CandidateType } from './CandidateList';

import VotingArtifact from '../../build/contracts/Voting.json';

const Voting = Contract(VotingArtifact);

const styles = StyleSheet.create({
	box: { paddingVertical: 0 },
});

type Props = *;
type State = {
	candidates: Array<CandidateType>,
};
class Main extends Component<Props, State> {
	state = {
		candidates: [],
	};
	componentWillMount() {
		Voting.setProvider(this.props.web3.currentProvider);
		this.setState({ coinbase: this.props.web3.eth.coinbase });
	}
	componentDidMount() {
		this.populateCandidates();
		const refreshBalances = () => {
			this.getCandidateBalances();
		};

		refreshBalances();

		setInterval(() => {
			refreshBalances();
			return refreshBalances;
		}, 5000);
	}

	getCandidateBalances: Function;
	getCandidateBalances = () => {
		/* We store the candidate names as bytes32 on the blockchain. We use the
		* handy toUtf8 method to convert from bytes32 to string
		*/
		const promiseN = this.state.candidates.map(({ hash }) => {
			return this.getCandidateVote(hash).then(votes => {
				return {
					hash,
					name: this.props.web3.toUtf8(hash),
					votes: votes.toString(),
				};
			});
		});
		Promise.all(promiseN).then(candidates => {
			this.setState({
				candidates,
			});
		});
	};
	getCandidateVote: Function;
	getCandidateVote = hash => {
		return Voting.deployed()
			.then(zetta => {
				return zetta.totalVotesFor.call(hash);
			})
			.catch(function(e) {
				console.log(e);
				throw e;
			});
	};

	/* Instead of hardcoding the candidates hash, we now fetch the candidate list from
	* the blockchain and populate the array. Once we fetch the candidates, we setup the
	* table in the UI with all the candidates and the votes they have received.
	*/
	populateCandidates: Function;
	populateCandidates = () => {
		return Voting.deployed().then(contractInstance => {
			return contractInstance.allCandidates.call().then(raw => {
				const candidates = raw.map(hash => {
					return {
						hash,
					};
				});
				this.setState({
					candidates,
				});
			});
		});
	};

	render() {
		return (
			<View style={styles.box}>
				<TokenData Voting={Voting} />
				<CandidateList Voting={Voting} candidates={this.state.candidates} />
			</View>
		);
	}
}

export default withWeb3(Main);
