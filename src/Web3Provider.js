/* @flow */

import React, { PureComponent } from 'react';

import Main from './Candidates/Main';
import Container from './Candidates/Container';

import { Broadcast } from 'react-broadcast';

class Web3Provider extends PureComponent<*> {
	render() {
		return (
			<Broadcast channel="web3" value={this.props.web3}>
				<Container>
					<Main />
				</Container>
			</Broadcast>
		);
	}
}

export default Web3Provider;
