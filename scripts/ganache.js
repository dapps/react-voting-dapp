'use strict';

const ganache = require('ganache-core');

const { port, network_id } = require('../truffle').networks.development;

const options = {
	port,
	network_id,
	logger: {
		log: message => {
			if (message && message.indexOf(' ') == 0) {
				console.log(message);
			}
		},
	},
};

var server = ganache.server();

server.listen(options.port, function(err, blockchain) {
	if (err) {
		return;
	}
	console.log('Ganache started successfully!');
});
process.on('uncaughtException', err => {
	console.log(err.stack || err);
	process.send({ type: 'error', data: err.stack || err });
});
