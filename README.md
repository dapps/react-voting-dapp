# react-voting-dapp

> A simple voting decentralized web app ( dapp ) for the ethereum blockchain, this is just in an early stage and can fail.

## Overview

* It's basically a dapp done using `react` and `web3`.

* This one can be considered as opinionated versions of [react-ethereum-dapp-template](https://github.com/uzyn/react-ethereum-dapp-template) taking solidity contracts from [ethereum_voting_dapp/chapter3](https://github.com/maheshmurthy/ethereum_voting_dapp/tree/master/chapter3) and [react-ethereum-metacoin](https://github.com/agrcrobles/react-ethereum-metacoin) intended for personal purposes, PRs, feedback, stars ✭ and issue reporting, welcome.

* When `Voting.sol` contract is deployed on the blockchain it initialized the list of super heroes as well as the total number of tokens available for "sale".

* Credits go also to [Mahesh Murthy's Medium article](https://medium.com/@mvmurthy/full-stack-hello-world-voting-ethereum-dapp-tutorial-part-1-40d2d0d807c2)

* More info recommend to read https://medium.com/blockchannel/life-cycle-of-an-ethereum-transaction-e5c66bae0f6e

## Getting started in localhost

JSON-RPC node should be running and configured in `truffle.js`.

For more information about it see [ganache-cli](https://github.com/trufflesuite/ganache-cli)

## Run the web server

* Node 8 LTS
* You can use yarn or npm

```bash
# Install dependencies
yarn

# Then launch the testpc server with
yarn server

# from a separate shell migrate solity contrats
yarn contracts:migrate

# and from a separate shell Run the web server
yarn web
```

## Dependencies

* [react](https://reactjs.org/) for building user interface
* [react-native-web](https://github.com/necolas/react-native-web) to handle native like components in the web
* [react-broadcast](https://github.com/ReactTraining/react-broadcast) to handle eficiently web3 in the component tree
* [web3](https://github.com/ethereum/web3.js) as the Etherum Javascript API
* and other cool ones like `react-native-typography`, `react-stars` etc

## Demo

![demo-example](https://gitlab.com/dapps/react-voting-dapp/raw/master/assets/P0CwBiv02k.gif)

## TODO

* Move to web3 version 1.0.0-beta to create the [instance of the contract](https://github.com/ethereum/wiki/wiki/JavaScript-API) and remove truffle-contract dependency as it done in [hello_world_dapp](https://github.com/mjhm/hello_world_dapp)

* Add react-native platform, currently blocked by [web3.js/issues/1022](https://github.com/ethereum/web3.js/issues/1022)


## License

MIT @ zetta